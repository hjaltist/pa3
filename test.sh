#!/bin/sh

a=1

until [ $a -gt 100 ]
do
	curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "New item", "year": "2009"}' localhost:1700
	curl --head localhost:1700 -sL -H 'Connection: close'
	curl localhost:1700
	curl -X PUT localhost:1700
	curl -X DELETE localhost:1700
done
