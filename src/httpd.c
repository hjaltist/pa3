#include <assert.h>
#include <string.h>
#include <netinet/in.h>
#include <glib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

//how many active connections we can have
#define MAX_CONNECTIONS 	500
//the max message size a client can send
#define MESSAGE_SIZE 		4096
//gadamn Windows line endings
#define SNL_CRLF 			"\r\n"
//gadamn Windows line endings
#define DNL_CRLF 			"\r\n\r\n"
//if we encounter error that means exit
#define EXIT_ERROR 			-1
//poll timeout to 2 sec
#define POLL_TIMEOUT 		(2 * 1000)
//socket timeout micro seconds
#define SOCKET_TIMEOUT 		30000000
//our server name
#define SERVER_NAME 		"HelloThereServer/1.0.0"
//certificate file
#define SSL_CERTIFICATE 	"certificate.pem"
//private key file file
#define SSL_PRIVATE_KEY 	"key.pem"
//passwords file
#define PASSWORDS_FILE 		"passwords.ini"
//salts file containing salt strings for users
#define SALT_FILE			"salt.ini"
//the length of the salt
#define SALT_LENGTH 20
//the length of the password hash
#define HASH_LENGTH 25
//how many times we iterate the hash
#define HASH_ITERATIONS 160000
//if a socket is http
#define HTTP 0
//if a socket is https
#define HTTPS 1

/////USEFUL KEYS/////
#define HEAD_REQUEST 		"HEAD"
#define GET_REQUEST 		"GET"
#define POST_REQUEST 		"POST"
#define REQUEST_KEY 		"request_type"
#define HTTP_VERSION_KEY 	"http_version"
#define METHOD_KEY 			"requested_method"
#define PAGE_KEY 			"viN8fADckVviN8fADckVt8SviN8fADckVt8jSYnjqFJYnjqFJ"

/////HEADER KEYS/////
#define COOKIE_HEAD			"Cookie"
#define AUTH_HEAD			"Authorization"

/////ENDPOINTS/////
#define COLOR_PAGE 			"/color"
#define TEST_PAGE 			"/test"
#define SECRET_PAGE 		"/secret"
#define LOGIN_PAGE 			"/login"

//an ssl connection , type 0 = HTTP, type 1 = HTTPS
//ssl is null if HTTP
struct ssl_connection{
	SSL * ssl;
	int type;
};

//the SSL context
SSL_CTX *ssl_context = NULL;
//keep a hold to our log file
FILE * log_file;
//our array of file descriptors
struct pollfd fds[MAX_CONNECTIONS];
//our array of SSL connections
struct ssl_connection ssl_connections[MAX_CONNECTIONS];
//our array of last active connections
gint64 time_since_active[MAX_CONNECTIONS];
//client and server structs
struct sockaddr_in server, https_server, client;
//port and main socket
int port, sockfd;
int https_port, https_sockfd;
//starts with 2 sockets then we increment when new
//connections are made
int number_of_fds = 2;
//valid salt inputs
static const char salt_input[] = "?!,.'-#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


/////FUNCTIONS/////
GHashTable * generate_header_table(gchar * header);
GHashTable * parse_uri(char * requested_path);
GHashTable * parse_cookie(char * cookies);
GString * html_response_with_data(char * data, GHashTable * table, int is_head);
GString * handle_request(char * message, GHashTable * headers, struct sockaddr_in client);
gchar * http_time(char * buffer);
gchar * http_connection_header_response(GHashTable * headers);
gchar* unique_salt();
int login_user(gchar * encoded_user_pass, gchar ** ret_username);
int create_user_pass(gchar * username, gchar * password);
void run();
void setupSSL();
void printer(char * message);
void print_hash_table(GHashTable * table);
void log_to_file(int port, char * address, char * request_method, char * host, int status_code, char * path);
void auth_log(int port, char * address, char * username, char * message, int to_out);
void check_timeouts();
void check_if_connection_should_be_closed(GHashTable * headers, int pos);
void close_connection(int pos);
void stop_blocking(int socket);
void new_connection(int socket, int pos, int is_https);
void old_connection(int pos, int is_https);
void resize_array(int pos);
void onExit(int signal);
//////////////////

int main(int argc, char * argv[]) {
	//create_user_pass("admin", "password");
	//for exiting
	signal(SIGINT, onExit);
	//need 3 arguments
	if (argc != 3) {
		printer("Received incorrect arguments. Expected: ./src/httpd port port+1");
		exit(EXIT_ERROR);
	}
	//try to open our log file, located in our root directory
	if ((log_file = fopen("./logfile.log", "a")) == NULL) {
		printer("Could not open log file.");
		printer("Nothing will be logged.");
	}
	//assign the port number to our http/https port variables
	sscanf(argv[1], "%i", &port);
	sscanf(argv[2], "%i", &https_port);

	/////////////////SETUP HTTP PORT////////////////
	//bind the socket and general setup
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0){
    	printer("socket() failed");
    	exit(EXIT_ERROR);
  	}
	//setup the server
	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);
	//bind the socket to the server
	if (bind(sockfd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) < 0){
		printer("bind() failed");
		exit(EXIT_ERROR);
	}
	//listen to the port and allow 250 connections on http
	if(listen(sockfd, (MAX_CONNECTIONS / 2)) < 0){
		printer("listen() failed");
		exit(EXIT_ERROR);
	}
	/////////////////END HTTP PORT///////////////

	/////////////////SETUP HTTPS PORT/////////////////
	//bind the socket and general setup
	https_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (https_sockfd < 0){
		printer("https socket() failed");
		exit(EXIT_ERROR);
	}
	//setup the server
	memset(&https_server, 0, sizeof(https_server));
	https_server.sin_family = AF_INET;
	https_server.sin_addr.s_addr = htonl(INADDR_ANY);
	https_server.sin_port = htons(https_port);
	//bind the socket to the server
	if (bind(https_sockfd, (struct sockaddr *) &https_server, (socklen_t) sizeof(https_server)) < 0){
		printer("https bind() failed");
		exit(EXIT_ERROR);
	}
	//listen to the port and allow 250 connections on https
	if(listen(https_sockfd, (MAX_CONNECTIONS / 2)) < 0){
		printer("https listen() failed");
		exit(EXIT_ERROR);
	}
	/////////////////END HTTPS PORT///////////////

	//configure and setup everything regarding SSL
	setupSSL();
	//init our poll array
	memset(fds, 0 , sizeof(fds));
	memset(ssl_connections, 0 , sizeof(ssl_connections));
	//assign our http socket the first slot
	fds[0].fd = sockfd;
  	fds[0].events = POLLIN;
	//assign our https socket the second slot
	fds[1].fd = https_sockfd;
	fds[1].events = POLLIN;
	//http does not use ssl
	ssl_connections[0].ssl =  NULL;
	ssl_connections[0].type = HTTP;
	//https uses ssl but client is acecepted without ssl
	ssl_connections[1].ssl = NULL;
	ssl_connections[1].type = HTTPS;
	//run the server
	run();

	return 0;
}

///runs the server in an endless while loop with poll
void run(){
	//loop and accept requests
	while (1) {
		//call poll and wait with a timeout of 5 minutes
		int poll_result = poll(fds, number_of_fds, POLL_TIMEOUT);
		if (poll_result < 0) {
			printer("poll() failed");
			break;
		}
		else if (poll_result == 0){
			//update our available sockets based on inactivity times
			check_timeouts();
			continue;
		}
		//loop and find readable file descriptors that are POLLIN
		for (int pos = 0; pos < number_of_fds; pos++) {
			//not allowed
			if(fds[pos].revents == 0) {
				continue;
			}
			//must be available
			if (fds[pos].revents == POLLIN) {
				//new connection through http or https
				if (fds[pos].fd == sockfd || fds[pos].fd == https_sockfd) {
					new_connection(fds[pos].fd, pos, (fds[pos].fd == https_sockfd));
				}
				//new data from already connected
				else {
					//serve the client and check if we should server through ssl or not.
					old_connection(pos, (ssl_connections[pos].type == HTTPS));
					//update the last active time
					time_since_active[pos] = g_get_monotonic_time();
				}
			}
		}
		//update our available sockets based on inactivity times
		check_timeouts();
	}
}

///sets up SSL for encrypted communications
void setupSSL(){
	//load encryption & hash algorithms for SSL
	SSL_library_init();
	//load the error strings for good error reporting
	SSL_load_error_strings();
	//add stuff
	OpenSSL_add_all_algorithms();
	//for a dedicated server
	if(!(ssl_context = SSL_CTX_new(SSLv23_server_method()))){
		printer(strerror(errno));
		printer("ERROR: SSL_CTX_new");
        exit(EXIT_ERROR);
	}
	//load server certificate into the SSL context
	if (SSL_CTX_use_certificate_file(ssl_context, SSL_CERTIFICATE, SSL_FILETYPE_PEM) <= 0) {
        printer("ERROR: Invalid certificate.");
        printer(strerror(errno));
        exit(EXIT_ERROR);
    }
	//load the server private-key into the SSL context
    if (SSL_CTX_use_PrivateKey_file(ssl_context, SSL_PRIVATE_KEY, SSL_FILETYPE_PEM) <= 0) {
        printer("ERROR: Invalid private key.");
        exit(EXIT_ERROR);
    }
}

//create a new connection
void new_connection(int socket, int pos, int is_https) {
	//to prevent a segmentation fault when we have to many open connections
	if (number_of_fds == MAX_CONNECTIONS) {
		return;
	}

	printer("Incoming new connection");
	socklen_t len = (socklen_t) sizeof(client);
	int connfd = accept(socket, (struct sockaddr *) &client, &len);
	if (connfd < 0){
		if (errno == EWOULDBLOCK) {
			printer("All connections up to date");
		} else {
			printer(strerror(errno));
		}
		return;
	}

	gchar * m = g_strdup_printf("Connection established for socket: %d", connfd);
	printer(m);
	g_free(m);

	//update the time when the connection was last active
	time_since_active[number_of_fds] = g_get_monotonic_time();
	//add the new connection to our structure
	fds[number_of_fds].fd = connfd;
	fds[number_of_fds].events = POLLIN;

	if (is_https){
		//assign the ssl
		ssl_connections[number_of_fds].ssl = SSL_new(ssl_context);
		//connect ssl to the connfd
		SSL_set_fd(ssl_connections[number_of_fds].ssl, connfd);
		ssl_connections[number_of_fds].type = HTTPS;
		//do the SSL handshake The SSL_accept() API waits for an SSL handshake initiation from the SSL client.
		//successful completion of this API means that the SSL handshake has been completed.
		if (SSL_accept(ssl_connections[number_of_fds].ssl) == -1) {
			printer("ERROR: SSL handshake to the client failed");
			close_connection(pos);
			return;
		}

		printer("SSL handshake successful");
		printer("");
	}
	else{
		ssl_connections[number_of_fds].ssl = NULL;
		ssl_connections[number_of_fds].type = HTTP;
	}

	//socket should not block
	stop_blocking(connfd);
	//increment current number of fds
	number_of_fds++;
}

///sending and receaving messages from known clients
void old_connection(int pos, int is_https) {
	char message[MESSAGE_SIZE];
	memset(&message, 0, sizeof(message));
	size_t size;
	//check if this is an ssl socket, should use https
	if (is_https){
		size = SSL_read(ssl_connections[pos].ssl, message, MESSAGE_SIZE);
	}
	else{
		size = recv(fds[pos].fd, message, MESSAGE_SIZE, 0);
	}

	if (size <= 0) {
		if (size == 0) {
			printer("Client disconnected");
			close_connection(pos);
			return;
		}
		if (errno != EWOULDBLOCK) {
			printer("SSL_read() error");
			close_connection(pos);
			return;
		}
	}

	printer("");
	gchar * m = g_strdup_printf("Sending requested data to socket: %d",fds[pos].fd);
	printer(m);
	g_free(m);
	//create a hash table that includes all the headers
	gchar ** head = g_strsplit(message, DNL_CRLF, 2);
	GHashTable * table = generate_header_table(head[0]);
	//handle the request received from the client
	GString * content = handle_request(message, table, client);

	size_t success;
	//check if this is an ssl socket, should use https
	if (is_https){
		success = SSL_write(ssl_connections[pos].ssl, content->str, content->len);
 	}
	else{
		success = send(fds[pos].fd, content->str, content->len, 0);
	}

	if (success == (size_t) -1) {
		printer("fails here");
		printer(strerror(errno));
		close_connection(pos);
	} else {
		gchar * mes = g_strdup_printf("Data requested for socket: %d successfully sent",fds[pos].fd);
		gchar * data_size = g_strdup_printf("Package size: %d", (int) content->len);
		printer(mes);
		printer(data_size);
		g_free(mes);
		g_free(data_size);
		//check if we should close the connection
		check_if_connection_should_be_closed(table, pos);
	}

	printer("");
	g_strfreev(head);
	g_string_free(content, TRUE);
	g_hash_table_destroy(table);
}

///returns the generated html with our data to be displayed
GString * html_response_with_data(char * data, GHashTable * page_table, int is_head) {
	GString * html = g_string_sized_new(1024);
	g_string_append(html, "<!DOCTYPE html>\n");
  	g_string_append(html, "<html>\n");
    g_string_append(html, "<head>\n");
    g_string_append(html, "<title>Assignment 3 Hyper Text Transfer Protocol</title>\n");
	g_string_append(html, "</head>\n");

	int is_color = g_strcmp0(g_hash_table_lookup(page_table, PAGE_KEY), COLOR_PAGE) == 0;
	int is_test = g_strcmp0(g_hash_table_lookup(page_table, PAGE_KEY), TEST_PAGE) == 0;

	if (!is_head) {
		if (is_color) {
			if (g_hash_table_contains(page_table, "bg") == TRUE) {
				gchar * color = g_strdup_printf("<body style=\"background-color:%s;\">\n",
						(gchar*)g_hash_table_lookup(page_table, "bg"));
				g_string_append(html, color);
				g_free(color);
			}
		} else {
			//if request is HEAD, skip the body
			g_string_append(html, "<body>\n");
			g_string_append(html, "<h1 style=\"color:black;\">\n");
			g_string_append(html, data);
			g_string_append(html, "\n");
			g_string_append(html, "</h1>\n");
			if (is_test) {
			    char * val;
			    char * key;
				GHashTableIter iter;
			    g_hash_table_iter_init(&iter, page_table);
				//iterate through the table with the request parameters
			    while (g_hash_table_iter_next(&iter, (gpointer) &key, (gpointer) &val)) {
			    	if (g_strcmp0(key, PAGE_KEY) == 0) {
			    		continue;
			    	}

					gchar * html_header = g_strdup_printf("<h2>%s=%s</h2>\n", key, val);
			        g_string_append(html, html_header);
					g_free(html_header);
			    }
			}
			g_string_append(html, "</body>\n");
		}
	}

	g_string_append(html, "</html>\n");

	return html;
}

///parse the URI send and put in a hash table the query paramters and the page
GHashTable * parse_uri(char * requested_path) {
	GHashTable * table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	//split the request into the page and the parameters
	char ** parsed = g_strsplit(requested_path, "?", 2);

	if (parsed != NULL){
		if (parsed[0] != NULL){
			//insert the page requested into the hash table for a specific predefined key
			g_hash_table_insert(table, g_strdup(PAGE_KEY), g_strdup(parsed[0]));
		}
		//if we have any query parameters we add them to the hash table
		if (parsed[1] != NULL) {
			//store all the paramters
			char * query_whole = parsed[1];
			//split them up by the & character
			char ** query_split = g_strsplit(query_whole, "&", -1);
			//loop and add all query paramters to the hash table
			for (int pos = 0; query_split[pos] != NULL; pos++) {
				//split the query parameters based on the =
				char ** key_value = g_strsplit(query_split[pos], "=", 2);
				if (key_value == NULL){
					continue;
				}
				//if it is null we just continue
				if (key_value[0] == NULL || key_value[1] == NULL) {
					continue;
				}
				//insert the query key and value
				g_hash_table_insert(table, g_strdup(key_value[0]), g_strdup(key_value[1]));
				//free the array
				g_strfreev(key_value);
			}

			g_strfreev(query_split);
		}
	}

	g_strfreev(parsed);

    return table;
}

///parse the cookie if it is in the header fields sent by the client
GHashTable * parse_cookie(char * cookies) {
	GHashTable * table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	//split on ; to create an array of all the cookies
	char ** values = g_strsplit(cookies, ";", -1);
	if(values != NULL) {
		//loop and add all cookies into a cookie hash table
		for (int pos = 0; values[pos] != NULL; pos++) {
			char ** key_value = g_strsplit(values[pos], "=", 2);
			//if it is null we just continue
			if (key_value[0] == NULL || key_value[1] == NULL) {
				continue;
			}
			//insert the query key and value
			g_hash_table_insert(table, g_strdup(key_value[0]), g_strdup(key_value[1]));
			//free the array
			g_strfreev(key_value);
		}
		g_strfreev(values);
	}

	return table;
}

///handles an incoming http request and creates a response
GString * handle_request(char * message, GHashTable * headers, struct sockaddr_in client) {
	//create our response
	GString * response = g_string_sized_new(1024);
	//it is of type HTTP/1.1.
	g_string_append(response, "HTTP/1.1 ");
	//look up the header table for the request type
	char * request_type = g_hash_table_lookup(headers, REQUEST_KEY);
	//look up the header table for the request path
	char * request_path = g_hash_table_lookup(headers, METHOD_KEY);
	//create our hash table that contains the page and query parameters
	GHashTable * page_table = parse_uri(request_path);

	//check which type of request this is
	int is_get = strcmp(request_type, GET_REQUEST) == 0;
	int is_post = strcmp(request_type, POST_REQUEST) == 0;
	int is_head = strcmp(request_type, HEAD_REQUEST) == 0;

	char buffer[50];
	memset(&buffer, 0, sizeof(buffer));

	gchar * time = http_time(buffer);
	gchar * date = g_strdup_printf("Date: %s\n", time);
	gchar * server = g_strdup_printf("Server: %s\n", SERVER_NAME);
	gchar * connection = http_connection_header_response(headers);

	gchar * status_message = "200 OK\n";
	GString * html = NULL;
	int generate_html = 1;
	int needs_auth = 0;

	//these are valid requests
	if (is_get || is_post || is_head) {
		//see if requested page is /secret
		int is_login = g_strcmp0(g_hash_table_lookup(page_table, PAGE_KEY), LOGIN_PAGE) == 0;
		int is_secret = g_strcmp0(g_hash_table_lookup(page_table, PAGE_KEY), SECRET_PAGE) == 0;
		//secret page requires authentication
		if (is_login || is_secret) {
			if (g_hash_table_contains(headers, AUTH_HEAD) == TRUE) {

				gchar * auth_string = g_hash_table_lookup(headers, AUTH_HEAD);
				gchar ** username_password = g_strsplit(auth_string, " ", 2);
				gchar * user_pass = username_password[1];
				gchar * username;
				int authenticate = login_user(user_pass, &username);

				if (!authenticate) {
					status_message = "401 Unauthorized\n";
					needs_auth = 1;
					generate_html = 0;
					html = html_response_with_data("401 Unauthorized", page_table, is_head);
					auth_log(ntohs(client.sin_port), inet_ntoa(client.sin_addr), username, "authentication error", 0);
				}
				else{
					auth_log(ntohs(client.sin_port), inet_ntoa(client.sin_addr), username, "authenticated", 1);
				}
				g_free(username);
				g_strfreev(username_password);
			} else {
				status_message = "401 Unauthorized\n";
				needs_auth = 1;
				generate_html = 0;
				html = html_response_with_data("401 Unauthorized", page_table, is_head);
			}
		}

		//the message returned inside the html
		GString * displayed_message = g_string_sized_new(1024);
		//the host is located like this -> Host: localhost:1600
		char * request_host = g_hash_table_lookup(headers, "Host");
		//create the response
		gchar * port = g_strdup_printf("%hu", ntohs(client.sin_port));
		//the default body reply if no other thing is specified e.g color page
		gchar * body = g_strdup_printf("http://%s%s %s:%s",request_host,request_path,inet_ntoa(client.sin_addr), port);
		g_string_append(displayed_message, body);

		//if a post request then we add the post paylod to our response
		if (is_post) {
			gchar ** post_data = g_strsplit(message, DNL_CRLF, 2);
			g_string_append(displayed_message, SNL_CRLF);
			g_string_append(displayed_message, post_data[1]);
			g_strfreev(post_data);
		}

		g_string_append(response, status_message);
		if (needs_auth && is_login) {
			g_string_append(response, "WWW-Authenticate: Basic realm=\"HelloThereServer\"\n");
		}
		g_string_append(response, "Content-Type: text/html\n");
		g_string_append(response, connection);
		g_string_append(response, date);
		g_string_append(response, server);

		//check if this is the color page so we can assign the cookies correctly if needed
		int is_color = g_strcmp0(g_hash_table_lookup(page_table, PAGE_KEY), COLOR_PAGE) == 0;
		//if this is the color page and we have a query bg pararmeter then we set the cookie for the color
		//that is then sent to the client.
		if (is_color && (g_hash_table_contains(page_table, "bg") == TRUE)) {
			gchar * set_cookie = g_strdup_printf("Set-Cookie: bg=%s\n", (gchar*)g_hash_table_lookup(page_table, "bg"));
			g_string_append(response, set_cookie);
			g_free(set_cookie);
		}
		//if no query parameter is specified but there is a cookie we insert the bg query parameter
		//into the page table which handles dipslaying the color in html_response_with_data
		else if (is_color && (g_hash_table_contains(page_table, "bg") == FALSE)) {
			//create our hash table that contains our cookies if there are any
			GHashTable * cookie_table = NULL;
			if (g_hash_table_contains(headers, COOKIE_HEAD) == TRUE) {
				cookie_table = parse_cookie(g_hash_table_lookup(headers, COOKIE_HEAD));
			}
			if (cookie_table != NULL && g_hash_table_contains(cookie_table, "bg") == TRUE) {
				g_hash_table_insert(page_table, g_strdup("bg"), g_strdup((gchar*)g_hash_table_lookup(cookie_table, "bg")));
				g_hash_table_destroy(cookie_table);
			}
		}

		if (generate_html) {
			//add the html to the response
			html = html_response_with_data(displayed_message->str, page_table, is_head);
		}
		//add the length of the html to the content length parameter
		gchar * content_length = g_strdup_printf("Content-Length: %ld\n", html->len);
		g_string_append(response, content_length);
		//finish with and empty line to indicate the end of the headers
		g_string_append(response, "\n");
		//append the html
		g_string_append(response, html->str);
		//log to our file
		log_to_file(ntohs(client.sin_port), inet_ntoa(client.sin_addr), request_type, request_host, 200, request_path);

		g_free(port);
		g_free(body);
		g_free(content_length);
		g_string_free(displayed_message, TRUE);
		g_string_free(html, TRUE);
	}
	//otherwise we respond with a 501 Not implemented error
	else {
		g_string_append(response, "501 Not implemented\n");
		g_string_append(response, "Content-Type: text/html\n");
		g_string_append(response, connection);
		g_string_append(response, date);
		g_string_append(response, server);
		//add the html to the response
		GString * html_501 = html_response_with_data("501 Not implemented. Please provide a valid request (GET | POST | HEAD).",
				page_table, is_head);
		//add the length of the error html to the content length parameter
		gchar * content_length = g_strdup_printf("Content-Length: %ld\n", html_501->len);
		g_string_append(response, content_length);
		//finish with and empty line to indicate the end of the headers
		g_string_append(response, "\n");
		//append the html error
		g_string_append(response, html_501->str);

		g_free(content_length);
		g_string_free(html_501, TRUE);
	}

	g_free(connection);
	g_free(date);
	g_free(server);
	g_hash_table_destroy(page_table);

	return response;
}

//create the hash table that holds our headers
GHashTable * generate_header_table(gchar * header) {
	GHashTable * table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	//split the lines based on new line
	char ** lines = g_strsplit(header, SNL_CRLF, INT_MAX);
	//get the request type e.g GET HTTP/1.1
	char ** request_type = g_strsplit(lines[0], " ", 3);
	g_hash_table_insert(table, g_strdup((gchar *) REQUEST_KEY), g_strdup(request_type[0]));
	g_hash_table_insert(table, g_strdup((gchar *) METHOD_KEY), g_strdup(request_type[1]));
	g_hash_table_insert(table, g_strdup((gchar *) HTTP_VERSION_KEY), g_strdup(request_type[2]));
	//loop through the headers and add to the table
	for (int pos = 1; lines[pos] != NULL; pos++) {
		//split the header based on the :
		char ** key_value = g_strsplit(lines[pos], ": ", 2);
		//if it is null we just continue
		if (key_value[0] == NULL || key_value[1] == NULL) {
			continue;
		}
		//insert the header as a key:value into the hash table
		g_hash_table_insert(table, g_strdup(key_value[0]), g_strdup(key_value[1]));
		//free the array
		g_strfreev(key_value);
	}
	//free the array
	g_strfreev(lines);
	g_strfreev(request_type);

	return table;
}

///the header field Connection
gchar * http_connection_header_response(GHashTable * headers){
	//http 1.0 sent to server defaults to close
	int http_10 = g_strcmp0(g_hash_table_lookup(headers, HTTP_VERSION_KEY), "HTTP/1.0") == 0;
	//something other sent than keep alive defaults to close
	int connection_keep_alive = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "keep-alive") == 0;
	//connection: close sent to server
	int connection_close = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "close") == 0;
	//http 1.0 defaults to connection:close unless it specifies keep alive
	if (http_10){
		if (connection_keep_alive){
			return g_strdup("Connection: keep-alive\nKeep-Alive: timeout=30\n");
		}
		return g_strdup("Connection: close\n");
	}
	//http 1.1 uses Connection:keep-alive unless it specifies close
	if (connection_close){
		return g_strdup("Connection: close\n");
	}

	return g_strdup("Connection: keep-alive\nKeep-Alive: timeout=30\n");
}

//check if we should close the connection after we send the response
void check_if_connection_should_be_closed(GHashTable * headers, int pos) {
	//connection: close sent to server
	int connection_close = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "close") == 0;
	//http 1.0 sent to server defaults to close
	int http_10 = g_strcmp0(g_hash_table_lookup(headers, HTTP_VERSION_KEY), "HTTP/1.0") == 0;
	//something other sent than keep alive defaults to close
	int connection_keep_alive = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "keep-alive") == 0;

	gchar * message_close = g_strdup_printf("Closing connection at %d",pos);

	//if this is http 1.1
	if (!http_10) {
		//if we get a connection close or something else than keep alive
		if (connection_close) {
			printer(message_close);
			close_connection(pos);
		}
		g_free(message_close);
		return;
	}

	if((http_10 && !connection_keep_alive) || (g_hash_table_contains(headers, "Connection") == FALSE)) {
		printer(message_close);
		close_connection(pos);
	}

	g_free(message_close);
}

//loop through active connections and check if we should close them
void check_timeouts() {
	for (int pos = 0; pos < number_of_fds; pos++) {
		if(time_since_active[pos] != 0){
			//calculate the time since last active
			gint diff = g_get_monotonic_time() - time_since_active[pos];
			//if it is too old we close the connection
			if (diff >= SOCKET_TIMEOUT) {
				char * timed_out = g_strdup_printf("Socket %d timed out.", fds[pos].fd);
				printer(timed_out);
				g_free(timed_out);

				//close the connection
				close_connection(pos);
			}
		}
	}
}

///return the http formatted time
gchar * http_time(char * buffer) {
	time_t now_time = time(0);
	strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", localtime(&now_time));
	return buffer;
}

///closes the connection and cleans up
void close_connection(int pos) {
	gchar * message = g_strdup_printf("Closing socket: %d", fds[pos].fd);
	printer(message);
	g_free(message);

	fds[pos].revents = 0;
	close(fds[pos].fd);
	//this could be NULl because we don't have an ssl connection at index 0.
	if (ssl_connections[pos].ssl != NULL){
		SSL_shutdown(ssl_connections[pos].ssl);
		SSL_free(ssl_connections[pos].ssl);
		//maybe free struct here?
	}
	resize_array(pos);
	printer("");
	if (number_of_fds == 1){
		printer("All connections have been closed");
	}
}

///creates a user in the "database"
int create_user_pass(gchar * username, gchar * password) {
	//init the password file
	GKeyFile * key_file = g_key_file_new();
	g_key_file_load_from_file(key_file, PASSWORDS_FILE, G_KEY_FILE_NONE, NULL);
	//only create the user if he does not exist
	gchar *user_exists = g_key_file_get_string(key_file, "passwords", username, NULL);
	if (user_exists != NULL){
		printer("Not able to create user: This user already exists.");
		g_free(user_exists);
		g_key_file_free(key_file);
		return 0;
	}

	//open salt and passwords file
	int fd = open(PASSWORDS_FILE, O_WRONLY | O_APPEND);
	int fd_salts = open(SALT_FILE, O_WRONLY | O_APPEND);
	gsize length;
	gsize salt_length;
	//create the salt
	gchar * salt = unique_salt();
	//init the salt file
	GKeyFile * salt_file = g_key_file_new();
	g_key_file_load_from_file(salt_file, SALT_FILE, G_KEY_FILE_NONE, NULL);

	//hash the password with PKCS5_PBKDF2_HMAC
	//we send in our password, send in the salt and
	//use sha256 hash function, we create a hash of length 20 with 160000 hash iterations.
	guchar hashed_password[HASH_LENGTH];
	PKCS5_PBKDF2_HMAC((char *)password, strlen(password), (guchar*)salt, strlen(salt), HASH_ITERATIONS, EVP_sha256(), HASH_LENGTH, hashed_password);
	//encode the password
	gchar * hashed_password_base64_encoded = g_base64_encode(hashed_password, HASH_LENGTH);
	//store the password
	g_key_file_set_string(key_file, "passwords", username, hashed_password_base64_encoded);
	//store the salt in the salt file under the unsername
	g_key_file_set_string(salt_file, "salts", username, salt);

	//the data to write to both files
	gchar * key_file_string = g_key_file_to_data(key_file, &length, NULL);
	gchar * salt_file_string = g_key_file_to_data(salt_file, &salt_length, NULL);
	//write to both files
	write(fd, key_file_string, length);
	write(fd_salts, salt_file_string, salt_length);

	g_free(key_file_string);
	g_free(salt_file_string);
	g_free(salt);
	g_free(hashed_password_base64_encoded);
	g_key_file_free(key_file);
	g_key_file_free(salt_file);

	close(fd);
	close(fd_salts);

	return 1;
}

///checks if a given authentication is present in our database
int login_user(gchar * encoded_user_pass, gchar ** ret_username) {
	//used for decode
	gsize length;
	//decode the authentication
	gchar * decodedAuth = (gchar*)g_base64_decode(encoded_user_pass, &length);
	//split it into user and password
	gchar ** decodedAuthArr =  g_strsplit(decodedAuth, ":", 2);
	//return if wrong format
	if (decodedAuthArr[0] == NULL || decodedAuthArr[1] == NULL){
		printer("User/Password format error");
		g_free(decodedAuth);
		g_strfreev(decodedAuthArr);
		return 0;
	}

	gchar * username = decodedAuthArr[0];
	gchar * password = decodedAuthArr[1];
	//use reference to assign the user name returned
	*ret_username = g_strdup(username);

	//init the salt file
	GKeyFile * salt_file = g_key_file_new();
	g_key_file_load_from_file(salt_file, SALT_FILE, G_KEY_FILE_NONE, NULL);
	//get the salt for the given user
	gchar * user_salt = g_key_file_get_string(salt_file, "salts", username, NULL);
	if (user_salt == NULL){
		g_free(decodedAuth);
		g_strfreev(decodedAuthArr);
		return 0;
	}

	//hash the password with PKCS5_PBKDF2_HMAC
	//we send in our password, send in the salt and
	//use sha256 hash function, we create a hash of length 20 with 160000 hash iterations.
	guchar hashed_password[HASH_LENGTH];
	PKCS5_PBKDF2_HMAC((char *)password, strlen(password), (guchar*)user_salt, strlen(user_salt), HASH_ITERATIONS, EVP_sha256(), HASH_LENGTH, hashed_password);
	//encode the password
	gchar * hashed_password_base64_encoded = g_base64_encode(hashed_password, HASH_LENGTH);

	//init the password file
	GKeyFile * key_file = g_key_file_new();
	g_key_file_load_from_file(key_file, PASSWORDS_FILE, G_KEY_FILE_NONE, NULL);

	//get the password for the given user
	gchar *stored_hashed_password_base64_encoded = g_key_file_get_string(key_file, "passwords", username, NULL);
	//compare the passwords
	int same_password = g_strcmp0(hashed_password_base64_encoded, stored_hashed_password_base64_encoded) == 0;

	g_free(decodedAuth);
	g_strfreev(decodedAuthArr);
	g_free(user_salt);
	g_free(hashed_password_base64_encoded);
	g_free(stored_hashed_password_base64_encoded);
	g_key_file_free(key_file);
	g_key_file_free(salt_file);

	//success/fail
	return same_password;
}

//log auth to the logfile

void auth_log(int port, char * address, char * username, char * message, int to_out) {
	//must have a log file open if we are logging to the file
	if (log_file == NULL && !to_out) {
		return;
	}
	//get the current time
	GTimeVal now;
	g_get_current_time(&now);
	now.tv_usec = 0;
	gchar * now_time = g_time_val_to_iso8601(&now);

	if(to_out){
		//print to stdout
		printf("%s : %s:%d %s %s\n", now_time, address, port, username, message);
	}
	else{
		//write to the logfile
		fprintf(log_file, "%s : %s:%d %s %s\n", now_time, address, port, username, message);
		fflush(log_file);
	}

	g_free(now_time);
}

///log to the logfile
void log_to_file(int port, char * address, char * request_method, char * host, int status_code, char * path) {
	//we must have an open file
	if (log_file == NULL){
		return;
	}
	//get the current time
	GTimeVal now;
	g_get_current_time(&now);
	now.tv_usec = 0;
	gchar * now_time = g_time_val_to_iso8601(&now);
	//write to the logfile
	fprintf(log_file, "%s : %s:%d %s\n%s%s : %d\n", now_time, address, port, request_method, host, path, status_code);
	fflush(log_file);
	g_free(now_time);
}

///print and newline and flush
void printer(char * message) {
	printf("%s",message);
	printf("\n");
	fflush(stdout);
}

///iterates over and prints a hash table
void print_hash_table(GHashTable * table) {
	GHashTableIter iter;
    char *val;
    char *key_;
    g_hash_table_iter_init (&iter, table);

    while (g_hash_table_iter_next (&iter, (gpointer) &key_, (gpointer) &val)){
        printf("key %s --> %s\n", key_, val);
        fflush(stdout);
    }
}

//socket should not block
void stop_blocking(int socket) {
	//get the options and then add the non block flag to
	//the options
	int opts = fcntl(socket,F_GETFL);
	if (opts < 0) {
		printer("Fetching socket options failed");
		exit(-1);
	}

	opts = (opts | O_NONBLOCK);

	if (fcntl(socket,F_SETFL,opts) < 0) {
		printer("Socket unblocking failed");
		exit(-1);
	}
}

//resizes our arrrays when sockets are closed
void resize_array(int pos) {
	// After a connection is closed, resize the fd array
	for(int i = pos; i < number_of_fds; i++) {
		if (i == number_of_fds - 1) {
			time_since_active[i] = 0;
			continue;
		}

		fds[i].fd = fds[i+1].fd;
		fds[i].events = fds[i+1].events;
		fds[i].revents = fds[i+1].revents;
		time_since_active[i] = time_since_active[i+1];
		ssl_connections[i] = ssl_connections[i+1];
	}

	number_of_fds--;
}

///exit the program and clean up
void onExit(int signal){
	printer("");
	gchar * message = g_strdup_printf("Signal %d received, Goodbye :)", signal);
	printer(message);
	g_free(message);

	for (int pos = 0; pos < number_of_fds; pos++) {
		close_connection(pos);
	}
	SSL_CTX_free(ssl_context);
	exit(EXIT_SUCCESS);
}

///generates a unique salt string for a specific user
gchar* unique_salt() {
	//create generator
    GRand* rand = g_rand_new();
	//malloc the size of our salt string
    gchar* salt = (gchar*)g_try_malloc0(SALT_LENGTH);
	//take the length of valid characters for the salt key
    int valid_length = strlen(salt_input);
	//generate a random string from our array of valid characters
    for (int x = 0; x < SALT_LENGTH; x++) {
        salt[x] = salt_input[g_rand_int_range(rand, 0, valid_length)];
    }

    return salt;
}
